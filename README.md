# Moved to GitHub

This module migrated to https://github.com/atlassian/jira-actions due to [JPERF-375](https://ecosystem.atlassian.net/browse/JPERF-375).